mongo-install:
	sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
	echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
	sudo apt-get update
	sudo apt-get install -y mongodb-org
	sudo service mongod start
	echo "==Started MongoDB=="
	sudo service mongod status
	echo "==Status MongoDB=="

mongo-auth:
	echo "==Create user admin MongoDB=="
	mongo < ./mongo/makeAdmin.js
	echo "==Created user admin MongoDB=="
	cp ./mongo/mongod.conf /etc/mongod.conf
	sudo service mongod restart
	echo "==Enable auth user admin MongoDB=="

docker-install:
	sudo apt-get update
	sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
	sudo apt-key fingerprint 0EBFCD88
	sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable"
	sudo apt-get update
	sudo apt-get install docker-ce
	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-Linux-x86_64" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
	echo "Install docker && docker-compose successful"

