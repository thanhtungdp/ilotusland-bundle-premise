declare -a IMAGES=( 
  "today108/ninhbinh-gateway-nginx"
  "today108/ilotusenviro-auth-api"
  "today108/ilotusenviro-category-api"
  "today108/ilotusenviro-station-auto-api"
  "today108/ilotusenviro-data-station-auto-api"
  "today108/envisoft-admin-api"
  "today108/ilotusenviro-role-api"
  "today108/envisoft-notify-api"
  "today108/ilotusenviro-fcm-api"
  "today108/ilotusenviro-database-api"
  "today108/ilotusenviro-support-api"
  "today108/ilotusenviro-aqi-api"
  "today108/ilotusenviro-wqi-api"
  "today108/envisoft-qa-api"
  "today108/ilotusenviro-station-fixed-api"
  "today108/ilotusenviro-data-station-fixed-api"
  "today108/ilotusenviro-camera-api"
  "today108/ilotusenviro-media-api"
  "today108/ilotusenviro-watch-api"
  "today108/ninhbinh-web-gis"
  "today108/envisoft-public-web"
  "today108/ninhbinh-web-admin"
  "today108/ilotusenviro-aqi-wqi-api"
)



for i in "${IMAGES[@]:0:5}"
do
   docker pull $i &
   # or do whatever with individual element of the array
done
wait

for i in "${IMAGES[@]:6:12}"
do
   docker pull $i &
   # or do whatever with individual element of the array
done
wait

for i in "${IMAGES[@]:13:18}"
do
   docker pull $i &
   # or do whatever with individual element of the array
done
wait

for i in "${IMAGES[@]:19:22}"
do
   echo  $i &
   # or do whatever with individual element of the array
done
wait